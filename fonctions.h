#ifndef FONCTIONS_H_INCLUDED
#define FONCTIONS_H_INCLUDED


/* Ptototypes des fonctions de dessins: */

void dessin_etoile(coords_t); /* fonction pour dessiner une �toile. */

void dessin_sapin(coords_t);  /* fonction pour dessiner un sapin. */

void dessin_chat(coords_t);   /* fonction pour dessiner un chat. */

#endif
