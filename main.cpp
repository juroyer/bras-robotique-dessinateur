
#include "opencv2/imgcodecs.hpp"
#include "opencv2/highgui.hpp"
#include "opencv2/imgproc.hpp"
#include <iostream>
//#include "serial.h"
//#include "rv-m1.h"

using namespace cv;
using namespace std;

Mat src_gray;
int thresh = 100;
RNG rng(12345);
vector<vector<Point> > contours;
vector<Vec4i> hierarchy;

void calculContours(int, void* );
void lancerDessin(int H, int L);

int main( int argc, char** argv ) {

    // Parsing d'arguments
    CommandLineParser parser( argc, argv, "{@input | ./X.jpg | image d'entree}" );
    Mat src = imread( parser.get<String>("@input") );
    if( src.empty() ) {
      cout << "Impossible de trouver ou d'ouvrir l'image\n" << endl;
      cout << "Usage : " << argv[0] << " <image>" << endl;
      return -1;
    }
    
    cvtColor( src, src_gray, COLOR_BGR2GRAY ); // Passage de l'image en noir et blanc / nuance de gris
    blur( src_gray, src_gray, Size(3,3) ); // Lissage pour enlever les imperfections / bruits / etc.
    
    // Création de la fenetre
    const char* fenetrePrincipale = "Source";
    namedWindow( fenetrePrincipale );
    imshow( fenetrePrincipale, src );
    const int max_thresh = 255;
    createTrackbar( "Sensibilite :", fenetrePrincipale, &thresh, max_thresh, calculContours );
    calculContours(0,0);


    waitKey(); // On appuie sur n'importe quelle touche pour demarrer le dessin reel par le bras
    lancerDessin(0, 0);
    return 0;
}

void calculContours(int, void* )
{
    Mat canny_output;
    Canny( src_gray, canny_output, thresh, thresh*2 );

    findContours( canny_output, contours, hierarchy, RETR_TREE, CHAIN_APPROX_SIMPLE );
    Mat dessinTemp = Mat::zeros( canny_output.size(), CV_8UC3 );

    for( size_t i = 0; i< contours.size(); i++ )
    {
        Scalar color = Scalar( rng.uniform(0, 256), rng.uniform(0,256), rng.uniform(0,256) );
        drawContours( dessinTemp, contours, (int)i, color, 2, LINE_8, hierarchy, 0 );
    }
    imshow( "Resultat", dessinTemp );
}

void lancerDessin(int H, int L) { // On mettra la fonction de controle de bras avec les bons parametres et peut-etre des delais entre les points (lol)
    cout << "Hello world!" << endl;
    for (vector<vector<Point> >::iterator chaine = contours.begin() ; chaine != contours.end(); ++chaine){
        for (vector<Point>::iterator trait = chaine->begin() ; trait != chaine->end(); ++trait){
            cout << " x : " << trait->x << ", y : " << trait->y << endl;
        }
    }
}