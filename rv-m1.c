#include "rv-m1.h"
#include <stdio.h>
#include <stdlib.h>
#include "serial.h"

/* Remet le bras a sa position d'origine */
void OG()
{
    char * str = "OG\n\n";
    writeSerial(str);
}

/* Ouvre la pince mecanique */
void GO()
{
    char * str = "GO\n\n";
    writeSerial(str);
}

/* Ferme la pince mecanique */
void GC()
{
    char * str = "GC\n\n";
    writeSerial(str);
}

/* Equivalent du bouton RESET */
void RS()
{
    char * str = "RS\n\n";
    writeSerial(str);
}

/* Change la vitesse du bras */
/* n allant de 1 a 9 */
void SP(int n)
{
    char str[10];
    if (n >= 1 && n <= 9)
    {
        sprintf(str, "SP %d\n\n", n);
        writeSerial(str);
    }
    else
    {
        printf("Erreur de selection de la valeur \n");
    }
}

/* Positionne le bras a l'emplacement enregistre */
/* n allant de 1 a 9 */
void MO(int n)
{
    char str[10];
    if (n >= 1 && n <= 9)
    {
        sprintf(str, "MO %d\n\n", n);
        writeSerial(str);
    }
    else
    {
        printf("Erreur de selection de la valeur \n");
    }
}

/* Positionne le bras a l'emplacement enregistre et met la pince dans
   l'etat desire */
/* n allant de 1 a 9 */
void MOG(int n, enum GSTATE gstate)
{
    char str[10];
    char gstate_char;

    if(gstate == OPEN)
    {
        gstate_char = 'O';
    }
    else
    {
        gstate_char = 'C';
    }

    if (n >= 1 && n <= 9)
    {
        sprintf(str, "MO %d, %c\n\n", n, gstate_char);
        writeSerial(str);
    }
    else
    {
        printf("Erreur de selection de la valeur \n");
    }
}

/* Enregistre la position courante du bras dans l'emplacement memoire n */
/* n allant de 1 a 9 */
void HE(int n)
{
    char str[10];
    if (n >= 1 && n <= 9)
    {
        sprintf(str, "HE %d\n\n", n);
        writeSerial(str);
    }
    else
    {
        printf("Erreur de selection de la valeur \n");
    }
}

/* Ramene le robot a la position predefinie precedente */
void DP()
{
    char * str = "DP\n\n";
    writeSerial(str);
}

/* Ramene le robot a la position predefinie suivante */
void IP()
{
    char * str = "IP\n\n";
    writeSerial(str);
}

/* Ramene le robot a son origine mecanique */
void NT()
{
    char * str = "NT\n\n";
    writeSerial(str);
}

/* Passe par toutes les positions enregistrees de n_start a n_end
   n_start et n_end allant de 1 a 9 */
void MC(int n_start, int n_end)
{
    char str[10];
    if (n_start >= 1 && n_start <= 9 &&
        n_end >= 1 && n_end <= 9)
    {
        sprintf(str, "MC %d, %d\n\n", n_start, n_end);
        writeSerial(str);
    }
    else
    {
        printf("Erreur de selection de la valeur \n");
    }
}

void WH(char * coords_str)
{
    char * str = "WH\n\n";
    writeSerial(str);
    readSerial(coords_str);
}

void MPC(coords_t coords)
{
    char str[40];
    sprintf(str, "MP %.1f,%.1f,%.1f,%.1f,%.1f\n\n", coords.x, coords.y, coords.z, coords.pitch, coords.roll);
    writeSerial(str);
}

void MP(float x, float y, float z, float pitch, float roll)
{
    char str[40];
    sprintf(str, "MP %.1f,%.1f,%.1f,%.1f,%.1f, %c\n\n", x, y, z, pitch, roll);
    writeSerial(str);
}

void PD(int n, coords_t coords)
{
    char str[40];
    sprintf(str, "PD %d,%.1f,%.1f,%.1f,%.1f,%.1f\n\n", n, coords.x, coords.y, coords.z, coords.pitch, coords.roll);
    writeSerial(str);
}

void MT(int n, float dist)
{
    char str[40];
    sprintf(str, "MT %d,%.1f\n\n", n, dist);
    writeSerial(str);
}

void MTG(int n, float dist, enum GSTATE gstate)
{
    char str[40];
    char gstate_char;

    if(gstate == OPEN)
    {
        gstate_char = 'O';
    }
    else
    {
        gstate_char = 'C';
    }

    sprintf(str, "MT %d,%.1f,%c\n\n", n, dist, gstate_char);
    writeSerial(str);
}

void DW(float x, float y, float z)
{
    char str[40];
    sprintf(str, "DW %.1f,%.1f,%.1f\n\n", x, y, z);
    writeSerial(str);
}

void MJ(float angle1, float angle2, float angle3, float pitch, float roll)
{
    char str[40];
    sprintf(str, "MJ %.1f,%.1f,%.1f,%.1f,%.1f\n\n", angle1, angle2, angle3, pitch, roll);
    writeSerial(str);
}

void NX()
{
    char * str = "NX\n\n";
    writeSerial(str);

}

void RC(int i)
{
    char str[40];
    sprintf(str, "RC %d\n\n", i);
    writeSerial(str);
}
