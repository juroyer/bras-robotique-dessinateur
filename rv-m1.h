#ifndef RVM1_H
#define RVM1_H

/**
 * Definit les coordonnees de la pince
 */
typedef struct coords_
{
    float x;
    float y;
    float z;
    float pitch; /*!< angle de l'embout */
    float roll; /*!< angle de la pince */
} coords_t;

/**
 * Definit l'etat de la pince
 */
enum GSTATE
{
    OPEN, /*!< pince ouverte */
    CLOSE /*!< pince fermee */
};

/**
 * @brief Remet le bras a sa position d'origine
 */
void OG();

/**
 * @brief Ouvre la pince mecanique
 */
void GO();

/**
 * @brief Ferme la pince mecanique
 */
void GC();

/**
 * @brief Equivalent du bouton RESET
 */
void RS();

/**
 * @brief Change la vitesse du bras
 * @param[in] n parametre de vitesse allant de 1 a 9
 */
void SP(int n);

/**
 * @brief Positionne le bras a l'emplacement enregistre
 * @param[in] n numero de la position enregistree allant de 1 a 9
 */
void MO(int n);

/**
 * @brief Positionne le bras a l'emplacement enregistre et met la pince dans
 * l'etat desire
 * @param[in] n numero de la position enregistree allant de 1 a 9
 * @param[in] gstate etat de la pince desire
 */
void MOG(int n, enum GSTATE gstate);

/**
 * @brief Enregistre la position courante du bras dans l'emplacement memoire n
 * @param[in] numero de l'emplacement memoire allant de 1 a 9
 */
void HE(int n);

/**
 * @brief Ramene le robot a la position predefinie precedente
 */
void DP();

/**
 * @brief Ramene le robot a la position predefinie suivante
 */
void IP();

/**
 * @brief Ramene le robot a son origine mecanique
 */
void NT();

/**
 * @brief Passe par toutes les positions enregistrees de n_start a n_end
 * @param[in] n_start numero de la position de depart allant de 1 a 9
 * @param[in] n_end numero de la position d'arrivee allant de 1 a 9
 */
void MC(int n_start, int n_end);

/**
 * @brief Ecrit dans coords_str les coordonnees actuelles de la pince
 * @param[out] coords_str la chaine contenant les coordonnees lues formatees
 * @param coords_str doit allouer au moins 26 chars
 */
void WH(char * coords_str);

/**
 * @brief Positionne la pince aux coordonnes indiquees
 * @param[in] coords les coordonnees ou la pince doit etre amenee
 */
void MPC(coords_t coords);

/**
 * @brief Positionne la pince aux coordonnes indiquees
 */
void MP(float x, float y, float z, float pitch, float roll);

/**
 * @brief Enregistre les coordonnees indiquees dans l'emplacement memoire indique
 * @param[in] n le numero de l'emplacement memoire allant de 1 a 9
 * @param[in] coords les coordonnees de la position
 */
void PD(int n, coords_t coords);

/**
 * @brief Bouge la pince d'une certaine distance dans la direction ou elle se trouvait
 * @param[in] n le numero de l'emplacement memoire allant de 1 a 9
 * @param[in] dist la distance
 */
void MT(int n, float dist);

/**
 * @brief Bouge la pince d'une certaine distance dans la direction ou elle se trouvait et met
 * la pince dans l'etat desire
 * @param[in] n le numero de l'emplacement memoire allant de 1 a 9
 * @param[in] dist la distance
 * @param[in] gstate etat de la pince desire
 */
void MTG(int n, float dist, enum GSTATE gstate);

/**
 * @brief Bouge la pince a partir de la position courante de la distance specifiee
 */
void DW(float x, float y, float z);

/**
 * @brief Bouge la pince des angles specifiees
 */
void MJ(float angle1, float angle2, float angle3, float pitch, float roll);

void RC(int i);

void NX();

#endif
