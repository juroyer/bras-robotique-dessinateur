#ifndef SERIAL_H
#define SERIAL_H

//#include <Windows.h>

/**
 * @brief Ouvre le port serie
 * @param[in] portname nom du port a ouvrir (ex: COM1)
 */
void openSerial(char *portname);

/**
 * @brief Ferme le port serie
 */
void closeSerial();

/**
 * @brief Ecrit dans le port qui a ete ouvert
 * @param[in] str chaine de caractere a ecrire dans le port
 * @return le nombre de caracteres ecrits
 */
int writeSerial(char *str);

/**
 * @brief Lit dans le port qui a ete ouvert
 * @param[out] buffer chaine de caractere ou ecrire la sortie du port
 * @return le nombre de caracteres lus
 */
int readSerial(char *buffer);

#endif // SERIAL_H
